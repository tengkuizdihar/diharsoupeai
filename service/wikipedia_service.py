import requests
import util.wikipedia_processors as wp


# TODO nanti bakal ngereturn semua article yang ditemukan di dalam sebuah dict
# typical url in wikipedia
def search_article(keyword: str, limit_search_result: int, page_number: int) -> list:
    search_result = requests.get(wp.build_search_url(keyword, limit_search_result, page_number))
    return wp.get_search_results_from_html(search_result.text)
