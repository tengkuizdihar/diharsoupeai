class ArticleData:
    """
    This class is a container for an article.
    Use vars(object) to convert this to a jsonable
    object.
    """
    def __init__(self, name: str, url: str, summary: str):
        self.name = name
        self.url = url
        self.summary = summary
    
    def __str__(self):
        name = "Title   : " + self.name
        url = "Url     : " + self.url
        summary = "Summary : " + self.summary
        return name + "\n" + url + "\n" + summary