import json
import service.wikipedia_service as ws
import logging
import random
from util import printresponse

# TODO change logging.DEBUG to logging.CRITICAL
def init_logging():
    logging.basicConfig(format="[%(asctime)s] %(levelname)s:    %(message)s", level=logging.CRITICAL)

if __name__ == "__main__":
    init_logging()

    while True:
        # User input
        search_term = input("What do you want to learn today? ")
        maximum_hits_number = int(input("What's the maximum number of hits for the search? "))
        
        # Processing starts here
        result = ws.search_article(search_term, maximum_hits_number, 0) 
        printresponse.print_item(random.choice(result))
        
        # If the user wants more item of the same topic
        while True:
            repeat_topic_confirm = input("Want more of the same topic? (y/n) ").lower()
            if  repeat_topic_confirm == "n":
                break
            printresponse.print_item(random.choice(result))

        # Exit here maybe
        continue_confirm = input("Would you like to know more? (y/n) ").lower()
        if continue_confirm == "n":
            break
