def add_border_decorator(print_func):
    BORDER = "==================================================="
    def bordered(item):
        print(BORDER)
        print_func(item)
        print(BORDER)
    return bordered

@add_border_decorator
def print_item(item):
    print(item)
