from bs4 import BeautifulSoup
import logging
from dataclass import ArticleData


def get_search_results_from_html(html: str) -> list:
    """
    This static function will give a list of all the result that's found
    inside of the page of a wikipedia search. The result would be a class
    called dataclasses. dataclass.ArticleClass
    
    The functionality of the search
    would be based on the page that's rendered through this url.
    url = "https://en.wikipedia.org/w/index.php?title=Special:Search&limit=100&offset=0&ns0=1&search=harry+potter&advancedSearch-current={}"
    """
    search_result = []
    
    logging.debug("Beginning to parse.")
    bs = BeautifulSoup(html, "lxml")
    logging.debug("Done parsing.")

    # get all the search result item in the page.
    search_list = bs.findAll(name="li", attrs={"class":"mw-search-result"})

    logging.debug("Beginning to process the soup.")
    for item in search_list:
        title = item.find(name="div", attrs={"class":"mw-search-result-heading"}).a["title"]
        url = "https://en.wikipedia.org" + item.find(name="div", attrs={"class":"mw-search-result-heading"}).a["href"]
        short_desc = item.find(name="div", attrs={"class":"searchresult"}).text
        search_result.append(ArticleData(title, url, short_desc))
    logging.debug("Finished processing soup.")
    
    return search_result

def build_search_url(query: str, limit_per_query: int, page_number: int) -> str:
    return "https://en.wikipedia.org/w/index.php?title=Special:Search&limit={0}&offset={1}&ns0=1&search={2}&advancedSearch-current={3}"\
        .format(limit_per_query, page_number, query, "{}")
